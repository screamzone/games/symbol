extends Node
# Manages all the attacks.


signal effect_placed(node, position)

var selected_attack: Attack setget ,_get_selected_attack
var selected_attack_index := -1

var _attack_list := {
	0: null,
	1: null,
	2: null,
}
var _last_attack_id := 0


func _ready():
	set_attack_at_index(0, preload("res://resources/attacks/repel.tscn").instance())
	set_attack_at_index(1, preload("res://resources/attacks/bolt.tscn").instance())


func clear_attack_at_index(index: int):
	_attack_list[index] = null


func deselect_attack():
	selected_attack_index = -1


func get_attack_at_index(index: int):
	return _attack_list[index]


func get_next_attack_id() -> int:
	_last_attack_id += 1
	return _last_attack_id


func place_effect(effect: AreaOfEffect, position: Vector2):
	emit_signal("effect_placed", effect, position)


func select_attack(index: int):
	selected_attack_index = index


func set_attack_at_index(index: int, attack: Attack):
	add_child(attack)
	attack.owner = self
	_attack_list[index] = attack


func _get_selected_attack():
	var index := selected_attack_index
	return _attack_list[index] if _attack_list.has(index) else null
