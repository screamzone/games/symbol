class_name SymbolIndicator
extends Control
# Indicates a symbol attack.


const DEFAULT_COLOUR := Color(0.0, 0.0, 0.0, 0.5)
const SELECTED_COLOUR := Color(0.8, 0.1, 0.1, 0.5)

export(String) var key := ""

var cooldown_length := 1.0
var selected := false

var _attack: Attack

onready var background_colour: ColorRect = $BackgroundColour
onready var cooldown: Control = $Cooldown
onready var cooldown_fg: Arc2D = $Cooldown/FG
onready var cooldown_label: Label = $Cooldown/Label
onready var cooldown_timer: Timer = $Cooldown/Timer
onready var icon: TextureRect = $Icon
onready var label: Label = $Label


func _process(_delta: float):
	selected = get_index() == AttackManager.selected_attack_index
	background_colour.color = SELECTED_COLOUR if selected else DEFAULT_COLOUR
	label.text = key
	_update_timer()


func setup(attack: Attack):
	_attack = attack
	if not attack:
		cooldown.visible = false
		icon.texture = null
		selected = false
		return

	icon.texture = attack.icon


func _update_timer():
	if not _attack or _attack.is_ready():
		cooldown.visible = false
		return

	var time_left := _attack.get_cooldown()
	cooldown.visible = true
	cooldown_fg.angle_size = (_attack.cooldown - time_left) / _attack.cooldown * 360
	cooldown_label.text = str(ceil(time_left))
