class_name Attack
extends Node2D
# A move used by an attacking character.


signal readied

const ORDER := [1, 2, 3, 4, 5, 6, 0]

export(String) var title := ""
export(int) var cooldown := 1
export(String, FILE, "*.tscn") var effect
export(Texture) var icon

var _cells := []

onready var tile_map: TileMap = $TileMap
onready var timer: Timer = $Timer
onready var _effect = load(effect)


func _ready():
	tile_map.visible = false


func determine_valid_cells(host: Node2D) -> Array:
	_cells = []
	_evaluate_cell(host, 1, 0)
	return _cells


func get_cooldown() -> float:
	return timer.time_left


func is_ready() -> bool:
	return timer.is_stopped()


func map_to_world_snapped(host: Node2D, x: int, y: int) -> Vector2:
	var global := host.position + to_global(Vector2(72 * x, 72 * y)) - Vector2(36, 36)
	return global.snapped(Vector2(72, 72)) + Vector2(36, 36)


func use(host: Node2D) -> bool:
	if not is_ready():
		return false

	var attack_instance_id = AttackManager.get_next_attack_id()
	for cell in determine_valid_cells(host):
		var effect_instance: AreaOfEffect = _effect.instance()
		effect_instance.attack_instance_id = attack_instance_id
		effect_instance.distance_from_firer = host.position.distance_to(cell)
		effect_instance.from_player = host is Player
		AttackManager.place_effect(effect_instance, cell)

	timer.wait_time = float(cooldown)
	timer.start()

	return true


func _evaluate_cell(host: Node2D, x: int, y: int):
	var tile_position = map_to_world_snapped(host, x, y)

	# We don't care about a cell if it's blocked or already in the array
	var collider = _get_collider(host, tile_position)

	if (collider and not(collider is KinematicBody2D)) or _cells.has(tile_position):
		return

	# Add to cells list
	_cells.append(tile_position)

	var cell_type = ORDER[tile_map.get_cell(x, y)]
	for value in range(0, 9):
		var offset_x: int = (value % 3) - 1
		var offset_y: int = int(floor(value / 3.0)) - 1

		if (offset_x == 0 and offset_y == 0) or (offset_x != 0 and offset_y != 0):
			continue

		var new_cell_type = ORDER[tile_map.get_cell(x + offset_x, y + offset_y)]
		if cell_type < new_cell_type:
			_evaluate_cell(host, x + offset_x, y + offset_y)


# Checks a cell for collisions
func _get_collider(host: CanvasItem, vec2: Vector2):
	var space_state := host.get_world_2d().direct_space_state
	var colliders := space_state.intersect_point(vec2)
	return colliders[0].collider if colliders.size() > 0 else null


func _on_Timer_timeout():
	emit_signal("readied")
