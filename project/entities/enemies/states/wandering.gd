extends State


export(float) var max_distance_to_wander := 512.0
export(float) var pause_time_between_wander := 1.0

var _original_position: Vector2


func enter():
	if not _original_position:
		_original_position = host.position

	call_deferred("_request_new_path")


func on_physics_process(_delta):
	if not host.scan_for_blockades():
		state_machine.swap("Chasing")


func on_path_completion():
	yield(get_tree().create_timer(pause_time_between_wander), "timeout")
	if state_machine.state == self:
		_request_new_path()


func _request_new_path():
	host.emit_signal("path_in_radius_requested", _original_position, max_distance_to_wander)
