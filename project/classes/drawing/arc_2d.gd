tool
class_name Arc2D
extends Node2D
# Draws an arc shape.


export(float) var angle_size := 270.0 setget _set_angle_size
export(Color) var color := Color.white setget _set_color
export(float) var radius := 32.0 setget _set_radius
export(int) var steps := 32 setget _set_steps
export(float) var thickness := 4.0 setget _set_thickness


func _draw():
	var points: PoolVector2Array = []

	for i in range(steps + 1):
		var angle := deg2rad(i * angle_size / steps)
		points.push_back(Vector2(cos(angle), sin(angle)) * radius)

	draw_polyline(points, color, thickness)


func _set_angle_size(value: float):
	angle_size = value
	update()


func _set_color(value: Color):
	color = value
	update()


func _set_radius(value: float):
	radius = value
	update()


func _set_steps(value: int):
	steps = value
	update()


func _set_thickness(value: float):
	thickness = value
	update()
