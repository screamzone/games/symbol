extends State


export(String, FILE, "*.tscn") var start_level

var _level: Level

onready var viewport: Viewport = $ViewportContainer/Viewport


func enter():
	if not start_level:
		printerr("Need to have a start level set on GameplayState!")
		return

	load_level(start_level)


func load_level(level_path: String):
	unload_level()

	_level = load(level_path).instance()
	viewport.add_child(_level)
	_level.owner = viewport

	_level.connect("change_requested", self, "load_level")


func unload_level():
	if not _level:
		return

	viewport.remove_child(_level)
	_level.queue_free()
