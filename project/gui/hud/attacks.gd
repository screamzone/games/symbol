extends HBoxContainer


onready var indicators: Array = get_children()


func _process(_delta):
	for indicator in indicators:
		indicator.setup(AttackManager.get_attack_at_index(indicator.get_index()))
