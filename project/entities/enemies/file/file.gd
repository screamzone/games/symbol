class_name File
extends "res://entities/enemies/enemy.gd"


onready var animated_sprite: AnimatedSprite = $AnimatedSprite


func _ready():
	animated_sprite.animation = "file_0" if randf() < .5 else "file_1"
