class_name AStarMap
extends TileMap


var astar: AStar2D = AStar2D.new()


func _ready():
	visible = false

	for cell in get_used_cells():
		var id := _get_id_for_point(cell)
		astar.add_point(id, cell)

		for x in range(-1, 2):
			for y in range(-1, 2):
				# Do not allow checking middle square or diagonals
				if abs(x) == abs(y):
					continue

				var to_id := _get_id_for_point(Vector2(cell.x + x, cell.y + y))
				if astar.has_point(to_id) and not astar.are_points_connected(id, to_id):
					astar.connect_points(id, to_id)


func get_points_in_radius(origin: Vector2, radius: float) -> PoolIntArray:
	var v1 = Vector2(origin.x - radius, origin.y - radius)
	var v2 = Vector2(origin.x + radius, origin.y + radius)
	return get_points_in_zone(v1, v2, true)


func get_points_in_zone(min_vec2: Vector2, max_vec2: Vector2, circular = false) -> PoolIntArray:
	var v1 := _to_map(min_vec2)
	var v2 := _to_map(max_vec2) + Vector2(1, 1)

	var middle := (v1 + v2) / 2
	var radius := (v2.x - v1.x) / 2

	var ids: PoolIntArray = []
	for x in range(v1.x + 1, v2.x):
		for y in range(v1.y + 1, v2.y):
			var pos = Vector2(x, y)
			if circular and pos.distance_to(middle) > radius:
				continue

			var id := _get_id_for_point(pos)
			if astar.has_point(id):
				ids.push_back(id)

	return ids


func get_random_path_from(from: Vector2, points = astar.get_points()) -> PoolVector2Array:
	var p = Array(points)
	p.shuffle()
	var id = astar.get_closest_point(_to_map(from))
	return _array_to_world(astar.get_point_path(id, p[0]))


func _array_to_map(array: PoolVector2Array) -> PoolVector2Array:
	var data: PoolVector2Array = []
	for point in array:
		data.push_back(_to_map(point))
	return data


func _array_to_world(array: PoolVector2Array) -> PoolVector2Array:
	var data: PoolVector2Array = []
	for point in array:
		data.push_back(_to_world(point))
	return data


func _get_id_for_point(vec2: Vector2) -> int:
	return (int(vec2.x) + 4096) + (int(vec2.y) + 4096) * 4096


func _to_map(vec2: Vector2) -> Vector2:
	return (vec2 / Vector2(72, 72)).floor()


func _to_world(vec2: Vector2) -> Vector2:
	return vec2 * Vector2(72, 72) + Vector2(36, 36)
