extends Node
# Cache of game events.


const LINE_COUNT := 5
const LOG_ENEMIES_DAMAGE = "%s enemies took a total of %s damage."
const LOG_ENEMY_DAMAGE = "An enemy took %s damage."

var lines := []

var _cache := {
	"enemy_casts": [],
	"enemy_damage": [],
	"player_casts": [],
	"player_damage": [],
}


func _ready():
	for i in range(0, LINE_COUNT):
		lines.append(_generate_new_line("", 0))


func _physics_process(_delta):
	_process_enemy_casts()
	_process_enemy_damage()
	_process_player_casts()
	_process_player_damage()


func log_enemy_damage(damage: int):
	_cache.enemy_damage.append({
		"damage": damage,
	})


func _generate_new_line(text: String, timestamp := OS.get_system_time_msecs()):
	return {
		"text": text,
		"timestamp": timestamp,
	}


func _log(text: String):
	lines.pop_front()
	lines.push_back(_generate_new_line(text))


func _process_enemy_casts():
	pass


# Logs enemy damage if any is cached.
func _process_enemy_damage():
	var damage := 0
	var enemy_count: int = _cache.enemy_damage.size()

	for data in _cache.enemy_damage:
		damage += data.damage

	match enemy_count:
		0:
			return
		1:
			_log(LOG_ENEMY_DAMAGE % damage)
		_:
			_log(LOG_ENEMIES_DAMAGE % [enemy_count, damage])

	_cache.enemy_damage = []


func _process_player_casts():
	pass


func _process_player_damage():
	pass
