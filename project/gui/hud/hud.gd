extends Control


onready var _player_health_label: Label = $PlayerHealthLabel


func set_player_health(health: int):
	_player_health_label.text = "%s HP" % health
