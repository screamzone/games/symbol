extends Node


const GRID_SIZE := 72


func get_snapped_position(position: Vector2) -> Vector2:
	return (position / GRID_SIZE).floor() * GRID_SIZE
