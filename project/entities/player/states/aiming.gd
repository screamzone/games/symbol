extends "res://classes/state/state.gd"


func enter():
	host.grid_marker.global_position = GridHelper.get_snapped_position(host.position)
	host.grid_marker.show()
	host.indicators.show()
	_update_grid()


func exit():
	host.grid_marker.hide()
	host.indicators.hide()
	.exit()


func on_physics_process(_delta):
	_update_grid()

	if Input.is_action_just_pressed("ui_cancel"):
		AttackManager.deselect_attack()
		exit()

	host.process_attack_keys()

	if Input.is_action_just_pressed("ui_accept"):
		var attack := AttackManager.selected_attack
		if attack and attack.use(host):
			AttackManager.deselect_attack()
			exit()


func _update_grid():
	var attack = AttackManager.selected_attack
	attack.rotation = host.facing.angle()

	var data = attack.determine_valid_cells(host)
	var indicators = host.indicators.get_children()

	for child in indicators:
		var index = child.get_index()
		if index >= indicators.size() or index >= data.size():
			child.hide()
		else:
			child.show()
			child.global_position = data[index]
