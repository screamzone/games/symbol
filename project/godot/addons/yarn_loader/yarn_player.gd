class_name YarnPlayer, "yarn_player.png"
extends Node


signal command_triggered(command, arguments)
signal dialogue_triggered(speaker, text)
signal options_shown(titles)
signal playback_began
signal playback_ended

const YarnHelpers = preload("yarn_helpers.gd")

export(String, FILE, "*.yarn") var path = ""

var current_line: int
var current_node
var options := []
var overrides := []

onready var helpers := YarnHelpers.new()
onready var yarn := Yarn.new()


func _ready():
	yarn.open(path)


func do_next():
	if options.size() > 0:
		printerr("Cannot use do_next() while waiting for an option to be selected.")
		return

	if current_line == current_node.lines.size():
		emit_signal("playback_ended")
		return

	var line

	if overrides.size():
		line = overrides.pop_front()
	else:
		line = current_node.lines[current_line]
		current_line += 1

	match line.type:
		Yarn.LINE_TYPE.COMMAND:
			emit_signal("command_triggered", line.command, line.arguments)
			return

		Yarn.LINE_TYPE.CONDITIONAL:
			for c in line.data:
				if helpers.evaluate(c.condition):
					for i in range(c.lines.size(), 0, -1):
						overrides.push_front(c.lines[i - 1])
					break

		Yarn.LINE_TYPE.DIALOGUE:
			emit_signal("dialogue_triggered", line.speaker, line.text)

			var next_line = get_next()
			if not next_line or next_line.type != Yarn.LINE_TYPE.OPTIONS:
				return

		Yarn.LINE_TYPE.JUMP:
			play(line.target)
			return

		Yarn.LINE_TYPE.OPTIONS:
			options = line.options
			var option_list := []

			for id in range(0, options.size(), 1):
				var option = options[id]
				if option.has("condition") and not helpers.evaluate(option.condition):
					continue
				option_list.push_back({
					"id": id,
					"title": option.title
				})

			emit_signal("options_shown", option_list)
			return

		Yarn.LINE_TYPE.SET:
			_YarnStore.set(line.variable, helpers.evaluate(line.value))

	do_next()


func get_next():
	if overrides.size() > 0:
		return overrides[0]

	if current_line < current_node.lines.size():
		return current_node.lines[current_line]

	return null


func play(title: String):
	overrides = []
	var node = yarn.get_node_by_title(title)

	if not node:
		printerr("Cannot find node with title '%s'." % title)
		return

	current_line = 0
	current_node = node
	emit_signal("playback_began")
	do_next()

	_YarnStore.set_visited(title)


func select_option(id: int):
	if options.size() == 0:
		printerr("Tried to select option when no options displayed.")
		return

	if id < 0 or id >= options.size():
		printerr("Tried to select invalid option.")
		return

	var lines = options[id].lines
	for i in range(lines.size(), 0, -1):
		overrides.push_front(lines[i - 1])

	options = []
	do_next()
