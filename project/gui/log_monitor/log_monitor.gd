class_name LogMonitor
extends VBoxContainer
# Shows logs from the Log singleton.


var line_instances := []

onready var log_monitor_line = preload("log_monitor_line.tscn")


func _ready():
	for _i in range(0, Log.LINE_COUNT):
		var instance: LogMonitorLine = log_monitor_line.instance()
		add_child(instance)
		instance.owner = self
		line_instances.append(instance)


func _process(_delta):
	for instance in line_instances:
		var data = Log.lines[instance.get_index()]
		instance.text = data.text
		instance.timestamp = data.timestamp
