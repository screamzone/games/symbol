class_name AreaOfEffect
extends Area2D
# A tile which causes an effect on enemies.


export(float) var lifetime := .5
export(int) var damage := 0
export(float) var delay_by_distance := 0.0
export(float) var fade_time := .1
export(float) var slowdown := 0.0

var attack_instance_id: int
var distance_from_firer: float
var from_player: bool

onready var timer: Timer = $Timer
onready var tween: Tween = $Tween


func _ready():
	visible = false
	var delay := delay_by_distance * max(0, distance_from_firer - 72) / 1000
	yield(get_tree().create_timer(delay), "timeout")
	visible = true
	timer.wait_time = lifetime
	timer.start()
	_start_fade()


func _physics_process(_delta):
	for body in get_overlapping_bodies():
		if from_player and body is Enemy:
			body.apply_area_of_effect(self)


func _on_Timer_timeout():
	queue_free()


func _start_fade():
	modulate.a = 0
	tween.interpolate_property(self, "modulate:a", 0.0, 1.0, fade_time)
	tween.start()
	yield(tween, "tween_completed")
	tween.interpolate_property(self, "modulate:a", 1.0, 0.0, fade_time, 0, 2, lifetime - fade_time * 2)
	tween.start()
