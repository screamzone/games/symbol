class_name LogMonitorLine
extends Label
# Displays a line of the log in a LogMonitor.


const FADE_TIME := 1.0
const VISIBLE_TIME := 5.0

var timestamp := 0


func _process(_delta):
	var fade_start := timestamp + VISIBLE_TIME * 1000
	var phase = 1.0 - (OS.get_system_time_msecs() - fade_start) / (FADE_TIME * 1000)
	modulate.a = clamp(phase, 0.0, 1.0)
