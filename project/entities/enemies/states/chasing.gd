extends State


export(float, 0, 1) var cowardice := .1 # How likely an enemy is to run
export(int) var cowardice_start_health := 10 # How low health has to be before running


func enter():
	host.player_in_sight = true


func exit():
	host.given_player_position = null


func on_physics_process(_delta):
	if host.movement_path.size() > 0:
		return

	on_path_completion()


func on_path_completion():
	if host.scan_for_blockades() or not host.player_in_sight:
		state_machine.swap("Wandering")
		return

	host.emit_signal("path_in_radius_requested", host.given_player_position, 64)
