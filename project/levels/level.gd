class_name Level
extends Node
# A section of the game's world in which the player moves and gameplay happens.


signal change_requested(level_path)

onready var astar_map: AStarMap = $AStarMap
onready var areas_of_effect: Node = $AreasOfEffect
onready var camera: Camera2D = $Camera2D
onready var enemies: Node = $Enemies
onready var player: Player = $Player


func _ready():
	randomize()
	AttackManager.connect("effect_placed", self, "_on_AttackManager_effect_placed")
	for enemy in enemies.get_children():
		enemy.connect("damaged", Log, "log_enemy_damage")
		enemy.connect("path_in_radius_requested", self, "_on_Enemy_path_in_radius_requested", [enemy])


func _physics_process(delta):
	for enemy in enemies.get_children():
		var distance: float = enemy.position.distance_to(player.position)

		if distance <= enemy.max_distance_to_see_player:
			enemy.given_player_position = player.position
		else:
			enemy.player_in_sight = false


func _on_AttackManager_effect_placed(effect: AreaOfEffect, position: Vector2):
	areas_of_effect.add_child(effect)
	effect.owner = areas_of_effect
	effect.position = position


func _on_Enemy_path_in_radius_requested(origin: Vector2, radius: float, enemy: Enemy):
	var points := astar_map.get_points_in_radius(origin, radius)
	enemy.movement_path = astar_map.get_random_path_from(enemy.position, points)
