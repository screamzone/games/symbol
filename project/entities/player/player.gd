class_name Player
extends KinematicBody2D
# The primary element controlled by the player, encompassing their agency.


signal health_changed(health)

var facing := Vector2(1, 0)
var health := 100 setget _set_health
var intent := Vector2.ZERO

onready var grid_marker: Sprite = $GridMarker
onready var indicators: Node2D = $Indicators
onready var state_machine: StateMachine = $StateMachine
onready var _aiming_state: State = $StateMachine/Aiming
onready var _invincibility_timer: Timer = $InvincibilityTimer


func _physics_process(_delta: float):
	var value := fmod(_invincibility_timer.time_left * 5, 1)
	$Sprite.visible = value == 0 or value > .5
	_update_intent_and_facing()


func damage(value: int):
	if _invincibility_timer.time_left > 0:
		return

	_invincibility_timer.start()
	_set_health(health - value)


func process_attack_keys():
	var attack: int
	if Input.is_action_just_pressed("attack_1"):
		attack = 0
	elif Input.is_action_just_pressed("attack_2"):
		attack = 1
	elif Input.is_action_just_pressed("attack_3"):
		attack = 2
	else:
		return

	AttackManager.select_attack(attack)

	var selected_attack = AttackManager.selected_attack

	if not selected_attack:
		if state_machine.state == _aiming_state:
			_aiming_state.exit()
		return

	if not state_machine.state == _aiming_state:
		state_machine.push("Aiming")


func _set_health(value: int):
	health = max(0, value)
	emit_signal("health_changed", value)


func _update_intent_and_facing():
	intent = Vector2(
		Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left"),
		Input.get_action_strength("ui_down") - Input.get_action_strength("ui_up")
	)

	if intent.length() == 0:
		return

	if abs(intent.x) > abs(intent.y):
		facing = Vector2.LEFT if intent.x < 0 else Vector2.RIGHT
	else:
		facing = Vector2.UP if intent.y < 0 else Vector2.DOWN
