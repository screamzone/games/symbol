extends "res://classes/state/state.gd"


export(float) var speed := 100.0


func on_physics_process(_delta):
	host.move_and_slide(host.intent * speed)
	host.process_attack_keys()
