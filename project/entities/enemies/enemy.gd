class_name Enemy
extends KinematicBody2D
# Represents an antagonist.


signal damaged(damage)
signal destroyed
signal path_in_radius_requested(origin, radius)

const FLASH_TIME := .5

export(float) var base_speed := 54.0
export(int) var health := 50
export(float) var max_distance_to_see_player := 512.0

var applied_attack_instances := []
var given_player_position: Vector2
var movement_path := []
var player_in_sight := false

onready var flash_timer: Timer = $FlashTimer
onready var health_label: Label = $HealthLabel
onready var ray_cast: RayCast2D = $RayCast2D
onready var state_machine: StateMachine = $StateMachine


func _physics_process(delta: float):
	_update_health()
	_update_visibility()
	_update_movement(delta)


func apply_area_of_effect(effect):
	if applied_attack_instances.has(effect.attack_instance_id):
		return

	applied_attack_instances.append(effect.attack_instance_id)
	damage(effect.damage)


func damage(value: int):
	health = max(0, health - value)
	emit_signal("damaged", value)

	if health != 0:
		flash_timer.wait_time = FLASH_TIME
		flash_timer.start()
		return

	emit_signal("destroyed")
	queue_free()


func scan_for_blockades() -> bool:
	if not given_player_position:
		return true

	ray_cast.cast_to = given_player_position - position
	ray_cast.force_raycast_update()
	ray_cast.force_update_transform()
	return not (ray_cast.get_collider() is Player)


func _update_health():
	health_label.text = "%s HP" % health


func _update_movement(delta: float):
	if movement_path.size() == 0:
		return

	var goal: Vector2 = movement_path[0]

	var direction := (goal - position).normalized()
	var distance := min(base_speed * delta, position.distance_to(goal))

	var collision := move_and_collide(direction * distance)
	if collision:
		state_machine.state.on_path_completion()
		return

	if distance < .1:
		movement_path.pop_front()

		if movement_path.size() == 0:
			state_machine.state.on_path_completion()


func _update_visibility():
	if flash_timer.is_stopped():
		visible = true
		return

	visible = fmod(flash_timer.time_left * 4.5, 1) > .5
